# battery-level

Measuring and reporting battery levels

1. clone with 
```
git clone --recursive https://gitlab.com/Yonder-Dynamics/controls/battery-level.git
```

2. to run
```
cmake . && make && ./battery
```
