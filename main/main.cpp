#if not defined (__AVR__) || (__avr__)

#include "../serial-message/Message.h"
#include "../common/Battery.h"

#define numOfCells 3

int main() {
  using namespace std::chrono;
  Serial.begin("/dev/ttyUSB0", B230400);

  struct { char arr[15]; } str;
  Message<15> greeting;
  while (!greeting.read(str));

  int successes = 0;
  auto loopStart = high_resolution_clock::now(); 
  for (int i = 1;; i++) {
    bool data = true;
    Battery<numOfCells> batteries[] = {{}, {}};

    Message<1> request;
    Message<sizeof(batteries)> response;

    request.write(data);
    if (!response.read(batteries)) continue;

    successes++;
    duration<double> time = high_resolution_clock::now() - loopStart;
    std::cout << "Success Rate: " << (double)successes/(double)i << ", ";
    std::cout << "Loop Count: " << i << ", ";
    std::cout << "Rate (Hz): " << (double)i/time.count() << ", ";
    std::cout << "Voltages: [";
    for (auto battery: batteries)
      for (int n = 0; n < numOfCells; n++)
        std::cout << battery.voltages[n] << ", ";
    std::cout << "]" << std::endl;
    // usleep(100000);
  }
}

#endif
