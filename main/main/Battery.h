template <unsigned Cells>
struct Battery {
  unsigned char pins[Cells]{};
  float voltages[Cells]{};
  static constexpr float voltageFactor = 5.0 / 1024.0;

#if not defined (__AVR__) || (__avr__)
  Battery() {}
#else
  template <typename ...Pins>
  Battery(Pins... pins) {
    static_assert(Cells == sizeof...(Pins), "Mismatched cell count");
    unsigned char input[] = { pins... };
    for (int i = 0; i < Cells; i++)
      this->pins[i] = input[i];
  }

  void readVoltages() {
    for (int i = 0; i < Cells; i++)
      voltages[i] = (i + 1) * voltageFactor * analogRead(pins[i]);
    for (int i = Cells; i > 0; i--)
      voltages[i] -= voltages[i - 1];

    for (int i = 0; i < Cells; i++) {
      voltages[i] = i;
    }
  }
#endif
} __attribute__((packed));
