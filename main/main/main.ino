#include "Message.h"
#include "Battery.h"

Battery<3> batteries[] = {{ A0, A1, A2 }, { A3, A4, A5 }};

int n = 0;
bool state = false;

void setup() {
  Serial.begin(230400);

  Message<15> greeting;
  greeting.write("Hello computer");

  pinMode(13, OUTPUT);
}

void loop() {
  bool data;
  Message<1> request;
  request.read(data);

  for (auto& battery: batteries) battery.readVoltages();
  Message<sizeof(batteries)> response;
  response.write(batteries);

  if ((n++)%10 == 0) { // speed indicator
    state = !state;
    digitalWrite(13, state);
  }
}
